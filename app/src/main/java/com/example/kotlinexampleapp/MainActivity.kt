package com.example.kotlinexampleapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
;

    }

    override fun onClick(v: View) {
        Toast.makeText(this.applicationContext, "Push the tempo", Toast.LENGTH_LONG).show()
    }
}
