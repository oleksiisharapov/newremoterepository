package com.example.kotlinexampleapp.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinexampleapp.R
import com.example.kotlinexampleapp.data.Weather
import kotlinx.android.synthetic.main.list_fragment_layout.*

class ListFragment(val cities: List<Weather>) : Fragment() {


    lateinit var recyclerList: RecyclerView
    lateinit var listAdapter: RecyclerView.Adapter<*>
    lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        recyclerList.layoutManager = LinearLayoutManager(container.context)
        recyclerList.adapter = WeatherAdapter(cities, container.context)

        return inflater.inflate(R.layout.list_fragment_layout, container, false)

    }





}