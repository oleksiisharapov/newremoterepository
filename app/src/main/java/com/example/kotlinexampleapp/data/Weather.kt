package com.example.kotlinexampleapp.data

class Weather(val cityName: String, val temperature: Double){

    override fun toString(): String {
        return "$cityName $temperature"
    }
}